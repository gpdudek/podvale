import React from 'react'
import { addDecorator } from '@storybook/react'
import Theme from '../src/theme/mainTheme'
import { BrowserRouter } from 'react-router-dom'

addDecorator((story) => (
  <Theme>
    <BrowserRouter>{story()}</BrowserRouter>
  </Theme>
))
