import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css?family=Lora:400,400i,500,600,700&display=swap');
  
    *, *::before, *::after {
        margin: 0;
        padding: 0;
        box-sizing: inherit;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    html {
        box-sizing: border-box;
        font-size: 62.5% // 1 rem === 10px
    } 

    body {
        font-size: 1.6rem; 
        font-family: 'lora', san-serif;
        font-weight: 400;
        line-height: 1.6;
    }

    img {
        display: inline-block;
        max-width: 100%;
    }

    a {
        color: inherit;
        text-decoration: none;

        :hover,
        :active,
        :visited,
        :focus {
        color: inherit;
        text-decoration: none;
    }
}
  `
export default GlobalStyle
