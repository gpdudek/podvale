import { css } from 'styled-components'

const sizes = {
  lg: 1400,
  md: 1200,
  sm: 992,
  xs: 768,
}

export default Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label]}px) {
      ${css(...args)};
    }
  `
  return acc
}, {})
