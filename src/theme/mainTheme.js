import React from 'react'
import { ThemeProvider } from 'styled-components'
import media from './media'

const theme = {
  grey100: 'hsl(0, 0%, 96%)',
  grey200: 'hsl(0, 0%, 90%)',
  grey300: 'hsl(0, 0%, 70%)',
  black: 'hsl(0, 0%, 0%)',
  background: {
    greeny: 'hsl(50, 21.4% ,94.5%)',
    green: 'hsl(96.9, 18.8%, 27.1%)',
    green2: 'hsl(173.9, 42.5%, 49.8%)',
    pinky: 'hsl(0, 14.3%, 94.5%)',
    pink: 'hsl(323.9, 42.5%, 49.8%)',
    browny: 'hsl(23.9, 42.5%, 49.8%)',
    brown: 'hsl(2.4, 27.5%, 35.7%)',
  },
  fontWeight: {
    regular: 400,
    medium: 500,
    semiBold: 600,
    bold: 700,
  },
  fontSize: {
    xxs: '0.87rem',
    xs: '1.2rem',
    s: '1.4rem',
    m: '2.1rem',
    l: '3rem',
    xl: '6rem',
  },
  ...media,
}

const Theme = ({ children }) => <ThemeProvider theme={theme}>{children}</ThemeProvider>

export default Theme
