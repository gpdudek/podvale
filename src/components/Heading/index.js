import styled, { css } from 'styled-components'

const Heading = styled.h1`
  font-size: ${({ theme }) => theme.fontSize.xl};
  line-height: 1.2;
  font-weight: ${({ theme }) => theme.fontWeight.regular};
  margin-top: 2rem;

  ${({ theme }) => theme.md`
    font-size: ${({ theme }) => theme.fontSize.l};
  `}

  ${({ center }) =>
    center &&
    css`
      margin: ${({ marginTop }) => (marginTop ? '10rem' : '2rem')} auto 2rem auto;
      width: 70%;
    `}
`
export default Heading
