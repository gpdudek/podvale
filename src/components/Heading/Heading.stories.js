import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'

import Heading from './index.js'

export default {
  component: Heading,
  title: 'components/atoms/Heading',
  decorators: [withKnobs],
}

export const HeroHeading = () => {
  return <Heading>Home sweet Home</Heading>
}

export const CenterHeading = () => {
  return (
    <Heading center marginTop>
      Home sweet Home
    </Heading>
  )
}
