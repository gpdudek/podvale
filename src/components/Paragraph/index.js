import styled, { css } from 'styled-components'

const Paragraph = styled.p`
  font-size: ${({ theme }) => theme.fontSize.s};
  line-height: 1.8;
  font-weight: ${({ theme }) => theme.fontWeight.regular};

  ${({ small }) =>
    small &&
    css`
      font-size: ${({ theme }) => theme.fontSize.s};
      line-height: 1.5;
    `}

  ${({ medium }) =>
    medium &&
    css`
      font-size: ${({ theme }) => theme.fontSize.m};
      line-height: 1.8;
    `}

  ${({ center }) =>
    center &&
    css`
      margin: 2rem auto;
      width: 70%;
      font-size: ${({ theme }) => theme.fontSize.l};
    `}
    
  ${({ marginTop }) =>
    marginTop &&
    css`
      margin-top: ${({ marginTop }) => marginTop && '2rem'};
    `}

`

export default Paragraph
