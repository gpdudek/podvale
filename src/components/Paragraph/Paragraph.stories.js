import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'

import Paragraph from './index.js'

export default {
  component: Paragraph,
  title: 'components/atoms/Paragraphs',
  decorators: [withKnobs],
}

export const Medium = () => {
  return (
    <Paragraph medium>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo ducimus fuga
      repellendus illum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo
      ducimus fuga repellendus illum. Lorem ipsum dolor sit amet consectetur adipisicing elit.
      Suscipit nemo ducimus fuga repellendus illum
    </Paragraph>
  )
}

export const Small = () => {
  return (
    <Paragraph small>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo ducimus fuga
      repellendus illum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo
      ducimus fuga repellendus illum. Lorem ipsum dolor sit amet consectetur adipisicing elit.
      Suscipit nemo ducimus fuga repellendus illum
    </Paragraph>
  )
}

export const Center = () => {
  return (
    <Paragraph center>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo ducimus fuga
      repellendus illum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit nemo
      ducimus fuga repellendus illum. Lorem ipsum dolor sit amet consectetur adipisicing elit.
      Suscipit nemo ducimus fuga repellendus illum
    </Paragraph>
  )
}
