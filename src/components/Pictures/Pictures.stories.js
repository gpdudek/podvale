import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import { livingroom } from 'data'
import Pictures from './index.js'

export default {
  component: Pictures,
  title: 'components/atoms/Pictures',
  decorators: [withKnobs],
}

export const PicturesTemplate = () => {
  return <Pictures data={livingroom.slice(1, 7)} />
}

export const PicturesRibbon = () => {
  return <Pictures version="ribbon" data={livingroom.slice(1, 7)} />
}

export const PicturesOne = () => {
  return <Pictures version="one" data={livingroom.slice(1, 2)} />
}

export const PicturesThree = () => {
  return <Pictures version="three" data={livingroom.slice(1, 4)} />
}

export const PicturesFive = () => {
  return <Pictures version="five" data={livingroom.slice(1, 6)} />
}

export const PicturesEight = () => {
  return <Pictures version="eight" data={livingroom.slice(1, 9)} />
}
