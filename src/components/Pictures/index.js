import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  width: 92%;
  margin: ${({ marginTop }) => (marginTop ? '10rem' : '0')} auto 2rem auto;
`
const PictureRibbon = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: 1fr;
  grid-gap: 1.5rem;
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
`
const PictureOne = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(4, 5vw);
  grid-gap: 1.5rem;

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 9;
    grid-row-start: 1;
    grid-row-end: 4;
  }
`
const PictureThree = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(5, 5vw);
  grid-gap: 1.5rem;

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 4;
    grid-row-start: 1;
    grid-row-end: 6;
  }

  & :nth-child(2) {
    grid-column-start: 4;
    grid-column-end: 6;
    grid-row-start: 1;
    grid-row-end: 6;
  }

  & :nth-child(3) {
    grid-column-start: 6;
    grid-column-end: 9;
    grid-row-start: 1;
    grid-row-end: 6;
  }
`
const PictureFive = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(9, 5vw);
  grid-gap: 1.5rem;

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 4;
    grid-row-start: 1;
    grid-row-end: 5;
  }

  & :nth-child(2) {
    grid-column-start: 4;
    grid-column-end: 7;
    grid-row-start: 1;
    grid-row-end: 5;
  }

  & :nth-child(3) {
    grid-column-start: 7;
    grid-column-end: 9;
    grid-row-start: 1;
    grid-row-end: 5;
  }

  & :nth-child(4) {
    grid-column-start: 1;
    grid-column-end: 4;
    grid-row-start: 5;
    grid-row-end: 10;
  }

  & :nth-child(5) {
    grid-column-start: 4;
    grid-column-end: 9;
    grid-row-start: 5;
    grid-row-end: 10;
  }
`

const PictureSix = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(10, 5vw);
  grid-gap: 1.5rem;

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 3;
    grid-row-start: 1;
    grid-row-end: 3;
  }

  & :nth-child(2) {
    grid-column-start: 3;
    grid-column-end: 5;
    grid-row-start: 1;
    grid-row-end: 3;
  }

  & :nth-child(3) {
    grid-column-start: 5;
    grid-column-end: 9;
    grid-row-start: 1;
    grid-row-end: 7;
  }

  & :nth-child(4) {
    grid-column-start: 1;
    grid-column-end: 5;
    grid-row-start: 3;
    grid-row-end: 7;
  }

  & :nth-child(5) {
    grid-column-start: 1;
    grid-column-end: 5;
    grid-row-start: 7;
    grid-row-end: 11;
  }

  & :nth-child(6) {
    grid-column-start: 5;
    grid-column-end: 9;
    grid-row-start: 7;
    grid-row-end: 11;
  }
`
const PictureEight = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(9, 5vw);
  grid-gap: 1.5rem;

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 3;
    grid-row-start: 1;
    grid-row-end: 4;
  }

  & :nth-child(2) {
    grid-column-start: 3;
    grid-column-end: 5;
    grid-row-start: 1;
    grid-row-end: 5;
  }

  & :nth-child(3) {
    grid-column-start: 5;
    grid-column-end: 7;
    grid-row-start: 1;
    grid-row-end: 5;
  }

  & :nth-child(4) {
    grid-column-start: 7;
    grid-column-end: 9;
    grid-row-start: 1;
    grid-row-end: 5;
  }

  & :nth-child(5) {
    grid-column-start: 1;
    grid-column-end: 3;
    grid-row-start: 4;
    grid-row-end: 7;
  }

  & :nth-child(6) {
    grid-column-start: 1;
    grid-column-end: 3;
    grid-row-start: 7;
    grid-row-end: 10;
  }

  & :nth-child(7) {
    grid-column-start: 3;
    grid-column-end: 7;
    grid-row-start: 5;
    grid-row-end: 10;
  }

  & :nth-child(8) {
    grid-column-start: 7;
    grid-column-end: 9;
    grid-row-start: 5;
    grid-row-end: 10;
  }
`

const BigPhoto = styled.div`
  z-index: 20000;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: 0 auto;
  padding: 20px;
  display: ${({ active }) => (active ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  overflow: auto;
  opacity: ${({ active }) => (active ? 1 : 0)};
  background-color: ${({ active, theme }) => active && theme.black};
  transition: all 0.3s;
`

const CrossIcon = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  padding-top: 18px;
  padding-right: 13px;
  padding-bottom: 0;
  padding-left: 13px;
  width: 60px;
  height: 60px;
  color: white;
  cursor: pointer;
`

const CrossOneLine = styled.span`
  position: absolute;
  top: 8px;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 20px;
  height: 1px;
  background-color: white;
  transform: rotate(-45deg);
  cursor: pointer;
`
const CrossTwoLine = styled.span`
  position: absolute;
  top: 8px;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 20px;
  height: 1px;
  background-color: white;
  transform: rotate(45deg);
  cursor: pointer;
`

const LiView = styled.div`
  img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`
export default class Pictures extends React.Component {
  constructor() {
    super()
    this.state = {
      fullPhoto: null,
    }
  }

  setFullPhoto = (picture) => {
    this.setState({
      fullPhoto: picture,
    })
  }

  render() {
    const { data, version, marginTop, photo } = this.props
    const { fullPhoto } = this.state

    let display = null

    switch (version) {
      case 'ribbon':
        display = (
          <PictureRibbon>
            {data.map((picture, index) => (
              <LiView key={index}>
                <img
                  src={picture.src}
                  alt="indoor view"
                  onClick={() => this.setFullPhoto(picture)}
                />
              </LiView>
            ))}
          </PictureRibbon>
        )
        break

      case 'one':
        display = (
          <PictureOne>
            {data.map((picture, index) => (
              <LiView key={index}>
                <img
                  src={picture.src}
                  alt="indoor view"
                  onClick={() => this.setFullPhoto(picture)}
                />
              </LiView>
            ))}
          </PictureOne>
        )
        break

      case 'three':
        display = (
          <PictureThree>
            {data.map((picture, index) => (
              <LiView key={index}>
                <img
                  src={picture.src}
                  alt="indoor view"
                  onClick={() => this.setFullPhoto(picture)}
                />
              </LiView>
            ))}
          </PictureThree>
        )
        break

      case 'five':
        display = (
          <PictureFive>
            {data.map((picture, index) => (
              <LiView key={index}>
                <img
                  src={picture.src}
                  alt="indoor view"
                  onClick={() => this.setFullPhoto(picture)}
                />
              </LiView>
            ))}
          </PictureFive>
        )
        break

      case 'eight':
        display = (
          <PictureEight>
            {data.map((picture, index) => (
              <LiView key={index}>
                <img
                  src={picture.src}
                  alt="indoor view"
                  onClick={() => this.setFullPhoto(picture)}
                />
              </LiView>
            ))}
          </PictureEight>
        )
        break

      default:
        display = (
          <PictureSix>
            {data.map((picture, index) => (
              <LiView key={index}>
                <img
                  src={picture.src}
                  alt="indoor view"
                  onClick={() => this.setFullPhoto(picture)}
                />
              </LiView>
            ))}
          </PictureSix>
        )
    }

    return (
      <Container marginTop={marginTop}>
        <BigPhoto active={fullPhoto ? true : false}>
          {fullPhoto && <img src={fullPhoto.src} />}
          <CrossIcon onClick={() => this.setFullPhoto(null)}>
            <CrossOneLine />
            <CrossTwoLine />
          </CrossIcon>
        </BigPhoto>
        {display}
      </Container>
    )
  }
}
