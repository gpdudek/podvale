import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'

import Navbar from './index.js'

export default {
  components: Navbar,
  title: 'components/Navbar',
  decorators: [withKnobs],
}

export const Classic = () => {
  return <Navbar />
}
