import React from 'react'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

const ListWrapper = styled.div`
  z-index: 900;
  position: fixed;
  top: 0;
  left: 0;
  display: grid;
  grid-template-columns: 1fr 1fr;
  width: 100vw;
  background-color: ${({ scrolling }) => (scrolling ? 'white' : 'transparent')};
  border-bottom: ${({ scrolling, theme }) => (scrolling ? `1px solid ${theme.black}` : '0')};
  transition: ${({ scrolling }) => (scrolling ? `.2s` : '0')};
`

const InnerListWrapperLeft = styled.ul`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  align-items: center;
  padding: 3vh 4vw;

  ${({ theme }) => theme.md`
    grid-template-columns: 1fr;
    grid-template-row: repeat(4, 1fr);
  `}
`
const InnerListWrapperRight = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: 3vh 4vw;

  ${({ theme }) => theme.md`
    align-items: flex-start;
  `}
`

const LiElem = styled.li`
  list-style: none;
`

const NavLinkElem = styled(NavLink)`
  text-decoration: none;
  color: ${({ theme }) => theme.black};
  font-size: ${({ theme }) => theme.fontSize.s};

  &:hover {
    font-weight: ${({ theme }) => theme.fontWeight.bold};
  }
`
const NavLinkElemWhite = styled(NavLink)`
  text-decoration: none;
  color: ${({ scrolling, theme }) => (scrolling ? `${theme.black}` : 'white')};
  font-size: ${({ theme }) => theme.fontSize.s};

  &:hover {
    font-weight: ${({ theme }) => theme.fontWeight.bold};
  }
`

export default class Navbar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      scrolling: false,
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = (event) => {
    if (window.scrollY === 0 && this.state.scrolling === true) {
      this.setState({ scrolling: false })
    } else if (window.scrollY !== 0 && this.state.scrolling !== true) {
      this.setState({ scrolling: true })
    }
  }

  render() {
    const { scrolling } = this.state
    return (
      <ListWrapper scrolling={scrolling}>
        <InnerListWrapperLeft>
          <LiElem>
            <NavLinkElem to="/">Home</NavLinkElem>
          </LiElem>
          <LiElem>
            <NavLinkElem to="/Pictures">Pictures</NavLinkElem>
          </LiElem>
          <LiElem>
            <NavLinkElem to="/Location">Location</NavLinkElem>
          </LiElem>

          <LiElem>
            <NavLinkElem to="/Landlords">Landlords</NavLinkElem>
          </LiElem>
        </InnerListWrapperLeft>
        <InnerListWrapperRight>
          <LiElem>
            <NavLinkElemWhite to="/Contact" scrolling={scrolling}>
              Contact
            </NavLinkElemWhite>
          </LiElem>
        </InnerListWrapperRight>
      </ListWrapper>
    )
  }
}
