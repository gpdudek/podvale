import React from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import TextPicture from './index.js'

export default {
  component: TextPicture,
  title: 'components/TextPicture',
  decorators: [withKnobs],
}

export const TextAndPicture = () => {
  return <TextPicture />
}
