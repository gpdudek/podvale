import React from 'react'
import styled from 'styled-components'

import Paragraph from 'components/Paragraph'
import Heading from 'components/Heading'

const HeroWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  border-bottom: 1px solid ${({ theme }) => theme.black};

  ${({ theme }) => theme.md`
    grid-template-columns: 1fr;
    grid-template-row: repeat(2, 1fr);
  `}
`
const LeftSite = styled.div`
  padding: 15vh 4vw;
  background-color: ${({ theme }) => theme.background.pinky};
  border-right: 1px solid ${({ theme }) => theme.black};

  ${({ theme }) => theme.md`
    padding-top: 30vh;
  `}
`
const TextWrapper = styled.div`
  margin: 10vh 0;
`

const Img = styled.img`
  display: block;
  width: 100%;
  object-fit: cover;
`

const TextPicture = ({
  headingHero,
  paragraphHero1,
  paragraphHero2,
  paragraphHero3,
  pictureHero,
}) => (
  <>
    <HeroWrapper>
      <LeftSite>
        <Heading marginTop>{headingHero}</Heading>
        <TextWrapper>
          <Paragraph marginTop medium>
            {paragraphHero1}
          </Paragraph>
          <Paragraph marginTop medium>
            {paragraphHero2}
          </Paragraph>
          <Paragraph marginTop>{paragraphHero3}</Paragraph>
          <Paragraph marginTop>podvale@palys-dudek.pl</Paragraph>
        </TextWrapper>
      </LeftSite>

      <Img src={pictureHero} alt="indoor design" />
    </HeroWrapper>
  </>
)

export default TextPicture
