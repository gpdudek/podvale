import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import GlobalStyle from 'theme/GlobalStyle'
import Theme from 'theme/mainTheme'

import Navbar from 'components/Navbar'
import HeroPage from './HeroPage'
import HistoryPage from './HistoryPage'
import LocationPage from './LocationPage'
import PicturesPage from './PicturesPage'
import ContactPage from './ContactPage'
import AboutUsPage from './AboutUsPage'

const Root = () => (
  <>
    <GlobalStyle />
    <Theme>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route exact path="/">
            <HeroPage />
          </Route>
          <Route path="/Pictures">
            <PicturesPage />
          </Route>
          <Route path="/Location">
            <LocationPage />
          </Route>
          <Route path="/Landlords">
            <AboutUsPage />
          </Route>
          <Route path="/History">
            <HistoryPage />
          </Route>
          <Route path="/Contact">
            <ContactPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Theme>
  </>
)

export default Root
