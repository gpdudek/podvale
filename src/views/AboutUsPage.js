import React from 'react'
import styled from 'styled-components'

import Paragraph from 'components/Paragraph'
import Heading from 'components/Heading'
import Pictures from 'components/Pictures'
import { mix } from 'data'

const Wrapper = styled.div`
  margin: 10rem 10rem;
  ${({ theme }) => theme.md`
    margin: 20rem 4vw;
  `}
`
const PicturesWrapper = styled.div`
  margin-top: 10rem;
  display: grid;
  grid-template-columns: repeat(6, 1fr);

  ${({ theme }) => theme.sm`
    grid-template-columns: repeat(3, 1fr);
    grid-template-row: repeat(2, 1fr);
  `}

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 3;
    grid-row-start: 1;
    grid-row-end: 2;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 4;
      grid-row-start: 1;
      grid-row-end: 2;
    `}
  }

  & :nth-child(2) {
    grid-column-start: 4;
    grid-column-end: 5;
    grid-row-start: 1;
    grid-row-end: 2;
    align-self: center;
    width: 128px;
    height: 128px;
    border-radius: 50%;
    ${({ theme }) => theme.sm`
      margin-top: 10rem;
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 2;
      grid-row-end: 3;
      width: 80px;
      height: 80px;
    `}
  }

  & :nth-child(3) {
    grid-column-start: 5;
    grid-column-end: 6;
    grid-row-start: 1;
    grid-row-end: 2;
    align-self: center;
    width: 128px;
    height: 128px;
    border-radius: 50%;
    ${({ theme }) => theme.sm`
      margin-top: 10rem;
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 2;
      grid-row-end: 3;
      width: 80px;
      height: 80px;
    `}
  }

  & :nth-child(4) {
    grid-column-start: 6;
    grid-column-end: 7;
    grid-row-start: 1;
    grid-row-end: 2;
    align-self: center;
    width: 128px;
    height: 128px;
    border-radius: 50%;
    ${({ theme }) => theme.sm`
      margin-top: 10rem;
      grid-column-start: 3;
      grid-column-end: 4;
      grid-row-start: 2;
      grid-row-end: 3;
      width: 80px;
      height: 80px;
    `}
  }
`

const TextPicture = styled.p`
  font-size: 2.4rem;
  line-height: 1.8;
  font-weight: 400;
`
const WrapperHeading = styled.div`
  text-align: center;
`

const AboutUs = () => (
  <Wrapper>
    <Paragraph center>
      We are two people who were living abroad for a couple of years. We changed our place every
      year, simply because we changed and our landlords didn't understand this and didn't want to
      adjust to our needs. The place where you live is a decent part of you, we believe, and we hope
      you both will change together, we will be happy to hear all your needs and proposal of
      solutions.
    </Paragraph>
    <PicturesWrapper>
      <TextPicture>
        Personally we are dog lovers and electric cars freakers. Be our guests.
      </TextPicture>
      <img src="assets/gosia_photo.png" />
      <img src="assets/michal_2020.png" />
      <img src="assets/IMG_1816.png" />
    </PicturesWrapper>
    <WrapperHeading>
      <Heading center marginTop>
        <a href="mailto:podvale@palys-dudek.pl">podvale@palys-dudek.pl</a>
      </Heading>
    </WrapperHeading>
    <Pictures marginTop data={mix.slice(1, 9)} version="ribbon" />
  </Wrapper>
)

export default AboutUs
