import React from 'react'
import styled, { css } from 'styled-components'

import Pictures from 'components/Pictures'
import TextPicture from 'components/TextPicture'
import { mix } from 'data'
import Heading from 'components/Heading'
import Paragraph from 'components/Paragraph'
import PicturesPage from './PicturesPage'
import LocationPage from './LocationPage'
import AboutUsPage from './AboutUsPage'

const WrapperHeading = styled.div`
  text-align: center;
  ${({ marginTop }) =>
    marginTop &&
    css`
      margin-top: ${({ marginTop }) => marginTop && '10rem'};
    `}
`

const HeroPage = () => (
  <>
    <TextPicture
      headingHero="New Home. New Chapter - Podvale"
      paragraphHero1="The home is a place where you want to come back anytime. Are you ready turn on your page?
      Is your next home?"
      paragraphHero2="Launching in the first day of May 2020 - brand new flat!"
      paragraphHero3="After complete refurbish, for long-term rent. Let us know if you're intrested:"
      pictureHero="/assets/Livingroom/IMG_1171.jpeg"
    />
    <WrapperHeading marginTop>
      <Paragraph center>Do you want to rent? Don't hesitate to write with any questions:</Paragraph>
      <Heading center>
        <a href="mailto:podvale@palys-dudek.pl">podvale@palys-dudek.pl</a>
      </Heading>
    </WrapperHeading>
    <Pictures marginTop data={mix.slice(0, 8)} version="ribbon" />
    <PicturesPage />
    <WrapperHeading>
      <Paragraph center>Do you want to rent? Don't hesitate to write with any questions:</Paragraph>
      <Heading center>
        <a href="mailto:podvale@palys-dudek.pl">podvale@palys-dudek.pl</a>
      </Heading>
    </WrapperHeading>
    <Pictures marginTop data={mix.slice(0, 8)} version="ribbon" />
    <LocationPage />
    <WrapperHeading>
      <Paragraph center>Do you want to rent? Don't hesitate to write with any questions:</Paragraph>
      <Heading center>
        <a href="mailto:podvale@palys-dudek.pl">podvale@palys-dudek.pl</a>
      </Heading>
    </WrapperHeading>
    <Pictures marginTop data={mix.slice(0, 8)} version="ribbon" />
    <AboutUsPage />
  </>
)

export default HeroPage
