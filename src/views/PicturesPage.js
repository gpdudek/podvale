import React from 'react'
import styled from 'styled-components'

import Pictures from 'components/Pictures'
import Heading from 'components/Heading'
import Paragraph from 'components/Paragraph'
import { livingroom, bathroom, kichen, hall, badroom, balcony, mix } from 'data'

const Wrapper = styled.div`
  margin: 10rem 10rem;
  ${({ theme }) => theme.md`
    margin: 20rem 1rem;
  `}
`

const PicturesPage = () => (
  <Wrapper>
    <Heading center>Home sweet home.</Heading>
    <Paragraph center>
      There is no place like a home. Podvale is THE Home... and Your home if you wish. Everyone who
      has been away for some time knows its value and the joy of coming back. Home is not cold
      walls, it's atmosphere, it's smell, it's colors and harmony that wakes us up every day and
      lets us fall asleep with peace. Podvale is a flat settle down in the heart of Wroclaw. So
      whenever you want to go - is walking distance.
    </Paragraph>
    <Pictures marginTop data={mix.slice(1, 7)} />

    <Heading center marginTop>
      Please come in to the livingroom.
    </Heading>
    <Paragraph center>
      Many people work at home. For some, it comes easily, for others it is a challenge. A
      comfortable double desk with good lighting and greenery outside the window will make it even
      easier to work more than one night :).
    </Paragraph>
    <Pictures marginTop data={livingroom.slice(0, 8)} version="eight" />
    <Pictures data={livingroom.slice(8, 13)} version="five" />
    <Pictures data={livingroom.slice(13, 18)} version="five" />
    <Pictures data={livingroom.slice(18, 21)} version="three" />

    <Heading center marginTop>
      And this is Kitchen.
    </Heading>
    <Paragraph center>
      Not everyone likes to cook, but the kitchen is often the center of life at home, the best
      parties take place here.
    </Paragraph>
    <Pictures marginTop data={kichen.slice(0, 5)} version="five" />
    <Pictures data={kichen.slice(5, 13)} version="eight" />
    <Pictures data={kichen.slice(13, 18)} version="five" />

    <Heading center marginTop>
      Please see badroom.
    </Heading>
    <Paragraph center>
      The bedroom is a place of dreams and visualization. A view of the opera and sometimes a
      concert from it will allow you to relax and add strength and smile in a difficult time. The
      best idea comes to mind in the middle of the night. There is a desk there so you can read a
      book there or do a make up.
    </Paragraph>
    <Pictures marginTop data={badroom.slice(8, 13)} version="five" />
    <Pictures data={badroom.slice(0, 8)} version="eight" />

    <Heading center marginTop>
      Please came in to the bathroom.
    </Heading>
    <Paragraph center>
      Remind of the old appearance of the house. Brick finish allows you to forget and separate
      moments.
    </Paragraph>
    <Pictures marginTop data={bathroom.slice(0, 8)} version="eight" />

    <Heading center marginTop>
      Hall.
    </Heading>
    <Paragraph center>
      This is what you can see whan you're back home. We put an extra affort to make it welcome and
      cosy.{' '}
    </Paragraph>
    <Pictures marginTop data={hall.slice(0, 3)} version="three" />
    <Pictures data={hall.slice(3, 8)} version="five" />
    <Pictures data={hall.slice(8, 16)} version="eight" />
    <Pictures data={hall.slice(16, 21)} version="five" />

    <Heading center marginTop>
      Balcony. The small garden.
    </Heading>
    <Paragraph center>
      Is Your private garden. Place to maditate or be colose to nature. Podvale really make it true.
    </Paragraph>
    <Pictures marginTop data={balcony.slice(0, 8)} version="eight" />
  </Wrapper>
)

export default PicturesPage
