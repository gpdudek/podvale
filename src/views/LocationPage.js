import React from 'react'
import styled from 'styled-components'

import Heading from 'components/Heading'
import Paragraph from 'components/Paragraph'

const Wrapper = styled.div`
  margin: 10rem;
  ${({ theme }) => theme.md`
    margin: 20rem 1rem;
  `}
`

const ImagesWraper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: 3;
  grid-gap: 1.5rem;

  ${({ theme }) => theme.sm`
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: 6;
  `}

  & :first-child {
    grid-column-start: 1;
    grid-column-end: 2;
    grid-row-start: 1;
    grid-row-end: 2;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 1;
      grid-row-end: 2;
  `}
  }

  & :nth-child(2) {
    grid-column-start: 2;
    grid-column-end: 3;
    grid-row-start: 1;
    grid-row-end: 2;

    ${({ theme }) => theme.sm`
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 1;
      grid-row-end: 2;
  `}
  }

  & :nth-child(3) {
    grid-column-start: 3;
    grid-column-end: 4;
    grid-row-start: 1;
    grid-row-end: 2;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 2;
      grid-row-end: 3;
  `}
  }

  & :nth-child(4) {
    grid-column-start: 4;
    grid-column-end: 5;
    grid-row-start: 1;
    grid-row-end: 2;

    ${({ theme }) => theme.sm`
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 2;
      grid-row-end: 3;
  `}
  }

  & :nth-child(5) {
    grid-column-start: 1;
    grid-column-end: 2;
    grid-row-start: 2;
    grid-row-end: 3;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 3;
      grid-row-end: 4;
  `}
  }

  & :nth-child(6) {
    grid-column-start: 2;
    grid-column-end: 3;
    grid-row-start: 2;
    grid-row-end: 3;

    ${({ theme }) => theme.sm`
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 3;
      grid-row-end: 4;
  `}
  }

  & :nth-child(7) {
    grid-column-start: 3;
    grid-column-end: 4;
    grid-row-start: 2;
    grid-row-end: 3;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 4;
      grid-row-end: 5;
  `}
  }

  & :nth-child(8) {
    grid-column-start: 4;
    grid-column-end: 5;
    grid-row-start: 2;
    grid-row-end: 3;

    ${({ theme }) => theme.sm`
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 4;
      grid-row-end: 5;
  `}
  }

  & :nth-child(9) {
    grid-column-start: 1;
    grid-column-end: 2;
    grid-row-start: 3;
    grid-row-end: 4;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 5;
      grid-row-end: 6;
  `}
  }

  & :nth-child(10) {
    grid-column-start: 2;
    grid-column-end: 3;
    grid-row-start: 3;
    grid-row-end: 4;

    ${({ theme }) => theme.sm`
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 5;
      grid-row-end: 6;
  `}
  }

  & :nth-child(11) {
    grid-column-start: 3;
    grid-column-end: 4;
    grid-row-start: 3;
    grid-row-end: 4;

    ${({ theme }) => theme.sm`
      grid-column-start: 1;
      grid-column-end: 2;
      grid-row-start: 6;
      grid-row-end: 7;
  `}
  }

  & :nth-child(12) {
    grid-column-start: 4;
    grid-column-end: 5;
    grid-row-start: 3;
    grid-row-end: 4;

    ${({ theme }) => theme.sm`
      grid-column-start: 2;
      grid-column-end: 3;
      grid-row-start: 6;
      grid-row-end: 7;
  `}
  }
`
const ImgMap = styled.div`
  img {
    padding: 3rem;
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;

    ${({ theme }) => theme.md`
      padding: 6px;
    `}
  }
`
const ImgMainMap = styled.div`
  margin: 10rem auto;
  display: flex;
  justify-content: center;
  img {
    flex-shrink: 0;

    border-radius: 50%;
    padding: 10px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  }
`

// eslint-disable-next-line react/prefer-stateless-function
class Location extends React.Component {
  render() {
    return (
      <Wrapper>
        <Heading center>Where it is?</Heading>
        <Paragraph center>
          Podvale is settle in the heart of Wroclaw. In the front of Wroclaw Opera.
        </Paragraph>
        <Paragraph center>
          Next to the Renoma shopping mall. Walking distance from market place (just 5 minutes
          walk).
        </Paragraph>
        <Paragraph center>
          Starbucks Green is nearby, with delicious coffee, cakes and sandwiches.
        </Paragraph>
        <ImgMainMap>
          <img src="/assets/map.png" alt="google map" />
        </ImgMainMap>
        <Heading center>Points of intrest.</Heading>
        <Paragraph center>Up to 10 minutes walk.</Paragraph>
        <ImagesWraper>
          <ImgMap>
            <img src="/assets/map1.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map2.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map3.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map4.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map5.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map6.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map7.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map8.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map9.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map10.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map11.png" alt="google map" />
          </ImgMap>
          <ImgMap>
            <img src="/assets/map12.png" alt="google map" />
          </ImgMap>
        </ImagesWraper>
      </Wrapper>
    )
  }
}

export default Location
