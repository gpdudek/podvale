const livingroom = [
  {
    src: `/assets/Livingroom/IMG_1171.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1222.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1228.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1229.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1233.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1236.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1244.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1247.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1250.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1254.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1256.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1260.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1272.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1265.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1268.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1262.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1275.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1277.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1284.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1276.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1285.jpeg`,
  },
]

const bathroom = [
  {
    src: `/assets/Bathroom/IMG_1180.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1181.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1177.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1178.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1179.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1173.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1175.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1183.jpeg`,
  },
]

const balcony = [
  {
    src: `/assets/Balcony/IMG_1208.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1210.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1211.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1217.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1215.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1218.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1213.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1246.jpeg`,
  },
]

const kichen = [
  {
    src: `/assets/Kichen/IMG_1312.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1315.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1325.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1332.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1319.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1323.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1329.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1316.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1340.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1349.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1320.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1337.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1338.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1330.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1346.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1348.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1328.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1350.jpeg`,
  },
]

const hall = [
  {
    src: `/assets/Hall/IMG_1352.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1288.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1188.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1191.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1193.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1192.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1189.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1197.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1197.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1198.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1199.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1202.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1206.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1207.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1263.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1184.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1290.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1296.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1292.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1187.jpeg`,
  },
  {
    src: `/assets/Hall/IMG_1353.jpeg`,
  },
]

const badroom = [
  {
    src: `/assets/Badroom/IMG_1170.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1220.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1221.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1298.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1304.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1300.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1301.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1302.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1299.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1305.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1307.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1309.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1311.jpeg`,
  },
]

const mix = [
  {
    src: `/assets/Hall/IMG_1352.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1332.jpeg`,
  },
  {
    src: `/assets/Badroom/IMG_1299.jpeg`,
  },
  {
    src: `/assets/Balcony/IMG_1210.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1254.jpeg`,
  },
  {
    src: `/assets/Kichen/IMG_1319.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1265.jpeg`,
  },
  {
    src: `/assets/Bathroom/IMG_1183.jpeg`,
  },
  {
    src: `/assets/Livingroom/IMG_1171.jpeg`,
  },
]

const data = [...livingroom, ...bathroom, ...kichen, ...hall, ...badroom, ...balcony, ...mix]

export default data

export { livingroom, bathroom, kichen, hall, badroom, balcony, mix }
